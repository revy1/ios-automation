@iOS @LoginFeature
Feature: [iOS] Login Feature

  @iOS @SuccessLogin
  Scenario: [iOS] User success login
    Given User click next button track app
    When User see entry point login
    And User click entry point login
    And User input username as "USERNAME_SOCIAL"
    And User input password as "PASSWORD_SOCIAL"
    And User click button login
    Then User see watchlist page

  @iOS @InvalidUsername
  Scenario: [iOS] User login with invalid username
    Given User click next button track app
    When User see entry point login
    And User click entry point login
    And User input username as "WRONG_USERNAME_SOCIAL"
    And User input password as "PASSWORD_SOCIAL"
    And User click button login
    Then User get invalid message

  @iOS @InvalidPassword
  Scenario: [iOS] User login with invalid password
    Given User click next button track app
    When User see entry point login
    And User click entry point login
    And User input username as "USERNAME_SOCIAL"
    And User input password as "WRONG_PASSWORD_SOCIAL"
    And User click button login
    Then User get invalid message

  @iOS @BlankUsernameandPassword
  Scenario: [iOS] User login with blank password and username
    Given User click next button track app
    When User see entry point login
    And User click entry point login
    And User input username as "BLANK_USERNAME_SOCIAL"
    And User input password as "BLANK_PASSWORD_SOCIAL"
    And User click button login
    Then User get error message blank username and password
