package steps_definition;

import io.cucumber.java8.En;
import page_object.LoginPage;

public class LoginSteps implements En {

    LoginPage loginpage = new LoginPage();
    public LoginSteps() {
        Given("User click next button track app", () -> loginpage.clickNextTrackApp());

        When("User see entry point login", () -> loginpage.isEntryPointLoginButton());

        And("User click entry point login", () -> loginpage.tapEntryPointLoginButton());

        And("^User input username as \"([^\"]*)\"$", (String username) -> loginpage.inputFieldUsername(username));

        And("^User input password as \"([^\"]*)\"$", (String password) -> loginpage.inputFieldPassword(password));

        And("User click button login", () -> loginpage.tapLoginButton());

        Then("User see watchlist page", () -> loginpage.isWatchlistPage());

        Then("User get error message blank username and password", () -> loginpage.blankUsernameandPassword());

        Then("User get invalid message", () -> loginpage.wrongUsernameandPassword());
    }
}