package utils;

import io.github.cdimascio.dotenv.Dotenv;
import page_object.BasePageObjects;

import javax.lang.model.util.Elements;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public class Utils {
    public static Properties ELEMENTS;
    public static void loadElementProperties(String directory) {
        File folder = new File(directory);
        File[] listOfFiles = folder.listFiles();
        ELEMENTS = new Properties();

        for (int i = 0; i < Objects.requireNonNull(listOfFiles).length; ++i) {
            if (listOfFiles[i].isFile() && listOfFiles[i].toString().contains(".properties")) {
                try {
                    ELEMENTS.load(new FileInputStream(directory + listOfFiles[i].getName()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static final Dotenv env = Dotenv.load();

    public static String env(String config) {
        String resolveEnvVariable = env.get(config.replace(" ","_").toUpperCase());
        if (resolveEnvVariable == null) {
            new BasePageObjects().printError(String.format("Env '%s' data not found! Please check your env file.", config));
        }
        return resolveEnvVariable;
    }
}