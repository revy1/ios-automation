package page_object;

import io.appium.java_client.AppiumBy;
import org.openqa.selenium.By;

import static utils.Utils.env;

public class LoginPage extends BasePageObjects {

    public void clickNextTrackApp() {
        tap("BUTTON_NEXT_PERMISSION");
    }

    public void isEntryPointLoginButton() {
        assertIsDisplayed("BUTTON_WELCOME_LOGIN_PAGE");
    }

    public void tapEntryPointLoginButton() {
        tap("BUTTON_WELCOME_LOGIN_PAGE");
    }

    public void inputFieldUsername(String text) {
        typeOn("FIELD_USERNAME_LOGIN", env(text)); //Revyta
    }

    public void inputFieldPassword(String password) {
        typeOn("FIELD_PASSWORD_LOGIN", env(password)); //stockbit
    }

    public void tapLoginButton () {
        tap("BUTTON_LOGIN");
    }

    public void isWatchlistPage() {
        assertIsDisplayed("POPUP_SMART_LOGIN_WATCHLIST");
    }

    public void wrongUsernameandPassword() {
        assertIsDisplayed("TOAST_WRONG_USERNAME_AND_PASSWORD");
    }

    public void blankUsernameandPassword() {
        assertIsDisplayed("TOAST_BLANK_PASSWORD_AND_USERNAME");
    }
}