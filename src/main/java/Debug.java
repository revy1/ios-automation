import static utils.Utils.env;

public class Debug {
    public static void main(String[] args) {
        System.out.println(env("USERNAME_SOCIAL"));
        System.out.println(env("PASSWORD_SOCIAL"));
        System.out.println(env("WRONG_USERNAME_SOCIAL"));
        System.out.println(env("WRONG_PASSWORD_SOCIAL"));
        System.out.println(env("BLANK_USERNAME_SOCIAL"));
        System.out.println(env("BLANK_PASSWORD_SOCIAL"));
    }
}
