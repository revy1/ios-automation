# Project ios-automation-bootcamp-task-2-4



## Dependencies

- Appium : Appium is an open-source test automation framework for mobile applications. It allows you to automate testing of mobile apps (both native and hybrid) across different platforms like iOS, Android.
- Cucumber :
  Cucumber is a popular tool used for behavior-driven development (BDD) and acceptance test-driven development (ATDD) in software development. It allows developers, testers, and business stakeholders to collaborate and define application behavior in plain text.
- Java : Java is a widely-used programming language in the field of automation testing. It provides various features and libraries that make it suitable for developing robust and efficient test automation frameworks.
- Selenium : Selenium is a widely-used open-source automation testing framework primarily used for web applications. It provides a set of tools and libraries to automate web browsers across different platforms.
## Setup Instruction and Tools
1. **Install Java JDK 17**: Download [here](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html)

2. **Install Homebrew** :
   Open Terminal and run the following command:

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```


2. **Install Node.js and npm**:
   Run the following commands in Terminal:

```
brew install node
```


3. **Install Appium 2**:
   Run the following command to install Appium using npm:

```
npm i --location=global appium
```
4. **Setup Cucumber** :
   Open the build.gradle file and add the necessary dependencies for Cucumber and any other libraries you may need.

```
dependencies {
      implementation 'io.cucumber:cucumber-java:7.3.1'
      implementation 'io.cucumber:cucumber-junit:7.3.1'
      testImplementation 'junit:junit:4.13.2'
   }
```

5. **Setup JUnit
   dependencies for JUnit**: This project using gradle, here are the steps: Open the build.gradle file of your Gradle project and add following dependencies for JUnit
```
dependencies {
      testImplementation 'junit:junit:4.12' 
    }
```

6. **Selenium** : This project using gradle, here are the steps: Open the build.gradle file of your Gradle project and add following dependencies for Selenium
   dependencies for Selenium
```
dependencies {
      implementation 'org.seleniumhq.selenium:selenium-java:4.13.0'
   }
``` 
## Usage Guide

1. **Run Appium**: Open terminal and input this following command
```
appium
```
2. **Open Intellij IDEA**
3. **Run file.feature**: that stored under the
```
/YourProject/src/test/resources/features
```


